class Component {
	tagName;
	children;
	attribute;
	constructor(tagName, attribute, children) {
		this.tagName = tagName;
		this.children = children;
		this.attribute = attribute;
	}
	render() {
		let chaine = '';
		if (this.children instanceof Array) {
			for (let index = 0; index < this.children.length; index++) {
				const element = this.children[index];
				if (element instanceof Component) {
					chaine += element.render();
				} else {
					chaine += element;
				}
			}
			this.children = chaine;
		} else if (this.children instanceof Component) {
		} else {
			chaine = this.children;
		}

		if (chaine == null && this.attribute != null) {
			return `<${this.tagName} ${this.renderAttribute()}/>`;
		} else if (this.attribute == null && chaine != null) {
			return `<${this.tagName}>${chaine}</${this.tagName}>`;
		} else {
			return `<${this.tagName} />`;
		}
	}
	renderAttribute() {
		if (this.attribute.name != null && this.attribute.value != null) {
			return `${this.attribute.name}=${this.attribute.value}`;
		}
	}
}
export default Component;
