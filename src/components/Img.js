import Component from './Component.js';
class Img extends Component {
	constructor(src) {
		super('img', { name: 'src', value: src }, null);
	}
}
export default Img;
